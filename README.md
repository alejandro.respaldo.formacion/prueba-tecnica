# Technical test

### 1. Objective of the technical test

The main purpose of the functionality developed in this project is to obtain a specific price, given a date, a brand
identifier, and a product identifier.

With these data, a database search will be carried out, which will return a list of prices that will later be filtered to 
obtain the price to be applied according to the date passed by parameter. If the result returns more than one price for 
that data, filtering is done by priority, an attribute that the price object has.

### 2. Explanation of architecture packages:

The functionality is programmed under the umbrella of the hexagonal architecture, having well differentiated the
responsibilities of each layer. Next, it is detailed what we can find inside each package of the
application:

* Kairos.pruebatecnica.domain: in it is the domain class of our application, in our case the class ***Price***.


* kairos.pruebatecnica.domain.exception: here, we find the class ***PriceNotFoundException*** that it is a custom 
exception to control the errors that may arise when performing the search.


* kairos.pruebatecnica.application.interfaces: in this package we find the interface ***PriceRepository*** which will be
the one that will serve as a port to communicate with the repository of the infrastructure layer that accesses the 
database.


* kairos.pruebatecnica.application.services: This is the module that contains the ***PriceUseCase*** class that contains
  the method *getPricebyBrandProductAndDate* which gives the actual functionality of the case of use of the application,
  and also serves as a port to connect to the restController of the infrastructure layer.


* kairos.pruebatecnica.infrastructure: here are the modules that connect us with the outside, inside we find the 
following modules:
  * config: in it we save the application configuration classes ***ConfigAplication*** where they will be declared
    the beans corresponding to the application layer, and the swagger configuration ***SwaggerConfig***
  * db: here is everything corresponding to data access, we find 3 submodules to have a better
    organized the responsibilities of each class.
    * dbo: we have hosted the ***PricesEntity*** class, which is the entity used to map our model
    towards the database.
    * mapper: In this package there is an auxiliary class that allows us to do the mapping between the Domain class and
    the data access entity class.
    * repository: inside we can find the interface ***PriceJpaRepository*** which is the one through its method
    *findByBrandIdAndProductId* will access the database and perform the search, also we have class ***PriceDbRepository*** 
    This class implements the domain interface to be able to communicate between layers. In turn, in this class, an 
    injection of the interface is made to access the data.
  * rest: is the package that is responsible for accepting http requests by the controller in this module we find
    the following modules:
    * apierror: The class ***PriceMessageError*** is hosted in it, which is the class we use to send the message
      response in case of search error.
    * controller: Here is the rest controller that contains the method that returns the price according to the 
    parameters passed by the url as path variables and query param, in the ***PriceController*** class we have an 
    attribute of the PriceUseCase type that is injected to have the connection between layers. We also have the class 
    ***PriceControllerAdvice*** which is responsible for listening for the default exception and sending the configured 
    error message.
    * dto: the ***PriceDto*** class is the one we use to send data through the http response.
    * mapper: we find the utility class ***PriceDtoMapper*** that is in charge of mapping the response that the method 
    gives from the PriceUseCase class to the response that the controller sends in the body of the http response.

### 3. Software requirements to run the application

To develop the software we have used the following technologies and versions:
* Java with jdk 11
* Springboot version 2.7.3
* Memory database h2
* Dependency manager with maven

For execute the application there is that follow this steps, in the shell terminal, in the root directory: 
1. Compile the project with maven (installed maven required in the laptop or computer)
~~~
    mvn compile
~~~
2. we create the .jar file with maven
~~~ 
    mvn package 
~~~
3. run the application with the follow command
~~~
    java -jar target/pruebatecnica-0.0.1-SNAPSHOT.jar
~~~

### 4. Test we have done

The tests that we have done, have been the unitary tests of the use case, and the integration tests of the controller.

### 5. Running Manual Tests:

The following manual tests can be performed to observe the operation of the application:

  1. Test run via postman:
     1. Run the application (steps on the point 3), uOnce the application is started, it stays listening on port 8080 of
        localhost.
     2.We generate a GET request to the route http://localhost:8080/prices/brands/{brandId}/products/{productId}
where the values of brandId and productId will be replaced by the value that we want to request.
Example url:
     ~~~
              http://localhost:8080/prices/brands/1/products/35455
     ~~~
     3. To add the queryParam can be done in two ways:
        1. writing in the route ?date= and the date with which the url would be as follows
     ~~~
     http://localhost:8080/prices/brands/1/products/35455?date=2020-06-14T10-00-00
     ~~~        
        2. the another way, within the params option create the parameter with the key 'date' and the value 2020-06-14T10-00-00
     4. Click send and wait for the response.
     5. possible responses for the request.
        1. response OK, status 200:
        ````
        {
        "brandId": 1,
        "starDate": "2020-06-14T00:00:00",
        "endDate": "2020-12-31T23:59:59",
        "productId": 35455,
        "price": 35.5
        }
        ````                
        2. response NOT_FOUND, status 404:
        
        ```
        {
        "message": "Precio no encontrado",
        "brandIdSearch": 2,
        "productIdSearch": 35455,
        "dateSearch": "2020-06-14T10:00:00"
        }
     
  2. Test run via swagger: 
     1. Run the application (steps on the point 3)
     2. We write the following url in the browser:
     ~~~
     http://localhost:8080/swagger-ui/index.html
     ~~~
     3. In the graphical interface that appears in the browser, the GET method that we have in the controller, if we 
     click on it is displayed, and we can make a request through from swagger to controller.
     4. We fill in the required fields
        1. For the brandId, fill in the corresponding value. Ex: 1
        2. For the productId, fill in the corresponding value. Ex: 35455
        3. For the date, fill in with the corresponding value. Ex: 2020-06-15T10:00:00Z 
     5. Click on execute, and observe the response 
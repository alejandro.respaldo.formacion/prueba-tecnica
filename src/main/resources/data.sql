--INSERT into brand(brand_text) values ('ZARA')

INSERT into price(brand_id, start_date, end_date, priority, product_id, price, curr) values (1, '2020-06-14T00.00.00', '2020-12-31 23.59.59', 0, 35455, 35.5, 'EUR')
INSERT into price(brand_id, start_date, end_date, priority, product_id, price, curr) values (1, '2020-06-14T15.00.00', '2020-06-14 18.30.00', 1, 35455, 25.45, 'EUR')
INSERT into price(brand_id, start_date, end_date, priority, product_id, price, curr) values (1, '2020-06-15T00.00.00', '2020-06-15 11.00.00', 1, 35455, 30.5, 'EUR')
INSERT into price(brand_id, start_date, end_date, priority, product_id, price, curr) values (1, '2020-06-15T16.00.00', '2020-12-31 23.59.59', 1, 35455, 38.95, 'EUR')

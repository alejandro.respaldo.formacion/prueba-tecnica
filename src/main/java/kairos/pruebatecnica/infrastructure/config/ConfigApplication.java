package kairos.pruebatecnica.infrastructure.config;

import kairos.pruebatecnica.application.interfaces.PriceRepository;
import kairos.pruebatecnica.application.services.PriceUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;



@Configuration
public class ConfigApplication {


    @Bean
    public PriceUseCase getPriceUseCase(PriceRepository priceRepository){
        return new PriceUseCase(priceRepository);
    }
}

package kairos.pruebatecnica.infrastructure.db.mapper;

import kairos.pruebatecnica.domain.Price;
import kairos.pruebatecnica.infrastructure.db.dbo.PricesEntity;

public class PriceEntityMapper {

    public static Price entityToDomain (PricesEntity price){
        return new Price(price.getStartDate(),price.getEndDate(), price.getPrice(), price.getCurrency(), price.getProductId(), price.getBrandId(), price.getPriority());
    }
}

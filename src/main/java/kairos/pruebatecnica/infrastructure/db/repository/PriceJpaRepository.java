package kairos.pruebatecnica.infrastructure.db.repository;

import kairos.pruebatecnica.infrastructure.db.dbo.PricesEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PriceJpaRepository extends CrudRepository<PricesEntity, Long> {

    List<PricesEntity> findByBrandIdAndProductId(Long brandId, Long productId);
}

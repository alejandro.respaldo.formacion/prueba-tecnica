package kairos.pruebatecnica.infrastructure.db.repository;

import kairos.pruebatecnica.application.interfaces.PriceRepository;
import kairos.pruebatecnica.domain.Price;
import kairos.pruebatecnica.infrastructure.db.mapper.PriceEntityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class PriceDbRepository implements PriceRepository {

    @Autowired
    PriceJpaRepository priceJpaRepository;
    @Override
    public List<Price> getPrices(Long brandId, Long productId) {

        return priceJpaRepository.findByBrandIdAndProductId(brandId, productId)
                .stream().map(PriceEntityMapper::entityToDomain)
                .collect(Collectors.toList());
    }
}

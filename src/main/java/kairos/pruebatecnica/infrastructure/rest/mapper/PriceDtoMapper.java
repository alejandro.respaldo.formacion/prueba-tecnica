package kairos.pruebatecnica.infrastructure.rest.mapper;

import kairos.pruebatecnica.domain.Price;
import kairos.pruebatecnica.infrastructure.rest.dto.PriceDto;
import org.springframework.stereotype.Component;

@Component
public class PriceDtoMapper {

    public PriceDto priceToPriceDto(Price price){
        return new PriceDto(price.getBrandId(), price.getStartDate(), price.getEndDate(), price.getProductId(), price.getPrice());
    }
}

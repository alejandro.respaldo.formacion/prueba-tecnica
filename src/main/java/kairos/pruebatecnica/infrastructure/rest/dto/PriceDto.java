package kairos.pruebatecnica.infrastructure.rest.dto;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
public class PriceDto {

    private Long brandId;

    private LocalDateTime starDate;

    private LocalDateTime endDate;

    private Long productId;

    private Double price;

}

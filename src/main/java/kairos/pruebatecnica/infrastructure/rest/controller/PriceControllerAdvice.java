package kairos.pruebatecnica.infrastructure.rest.controller;

import kairos.pruebatecnica.domain.exception.PriceNotFoundException;
import kairos.pruebatecnica.infrastructure.rest.apierror.PriceMessageError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class PriceControllerAdvice {

    @ExceptionHandler(PriceNotFoundException.class)
    public ResponseEntity<PriceMessageError> handleNotFoundPriceForApply(PriceNotFoundException exception){

        PriceMessageError messageError = PriceMessageError.builder()
                .message(exception.getMessage())
                .brandIdSearch(exception.getBrandId())
                .productIdSearch(exception.getProductId())
                .dateSearch(exception.getDate())
                .build();
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(messageError);
    }
}

package kairos.pruebatecnica.infrastructure.rest.controller;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import kairos.pruebatecnica.application.services.PriceUseCase;
import kairos.pruebatecnica.domain.Price;
import kairos.pruebatecnica.infrastructure.rest.dto.PriceDto;
import kairos.pruebatecnica.infrastructure.rest.mapper.PriceDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/prices")
public class PriceController {

    @Autowired
    private PriceUseCase priceUseCase;
    @Autowired
    private PriceDtoMapper mapperDto;
    @ApiOperation(value = "Obterner el precio para aplicar")
    @GetMapping(value = "/brands/{brandId}/products/{productId}")
    public ResponseEntity<PriceDto> getPricetoApply(@PathVariable(name = "brandId") Long brandId,
                                                    @PathVariable(name = "productId") Long productId,
                                                    @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME,
                                                            fallbackPatterns = "yyyy-MM-dd'T'HH-mm-ss")
                                                        LocalDateTime date){
        Price price = priceUseCase.getPricebyBrandProductAndDate(brandId,productId,date);
        PriceDto priceDtoResponse = mapperDto.priceToPriceDto(price);
        return new ResponseEntity(priceDtoResponse, HttpStatus.OK);
    }
}

package kairos.pruebatecnica.infrastructure.rest.apierror;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Builder
@Data
public class PriceMessageError implements Serializable {

    private String message;

    private Long brandIdSearch;
    private Long productIdSearch;

    private LocalDateTime dateSearch;
}

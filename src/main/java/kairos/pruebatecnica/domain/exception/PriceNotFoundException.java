package kairos.pruebatecnica.domain.exception;

import lombok.Getter;

import java.time.LocalDateTime;
@Getter
public class PriceNotFoundException extends RuntimeException{

    private Long brandId;
    private Long productId;
    private LocalDateTime date;

    public PriceNotFoundException(String message, Long brandId, Long productId, LocalDateTime date) {
        super(message);
        this.brandId = brandId;
        this.productId = productId;
        this.date = date;
    }
}

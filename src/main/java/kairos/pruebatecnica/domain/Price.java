package kairos.pruebatecnica.domain;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@AllArgsConstructor
@Getter
@Setter
public class Price {

    private LocalDateTime startDate;

    private LocalDateTime endDate;

    private Double price;
    private String currency;
    private Long productId;
    private Long brandId;
    private Integer priority;
}

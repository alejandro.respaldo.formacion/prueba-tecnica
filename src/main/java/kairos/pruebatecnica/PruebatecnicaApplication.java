package kairos.pruebatecnica;

import kairos.pruebatecnica.application.services.PriceUseCase;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
public class PruebatecnicaApplication {

	public static void main(String[] args) {

		SpringApplication.run(PruebatecnicaApplication.class, args);
	}

}

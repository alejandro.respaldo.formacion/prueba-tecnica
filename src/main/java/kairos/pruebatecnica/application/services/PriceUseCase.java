package kairos.pruebatecnica.application.services;

import kairos.pruebatecnica.application.interfaces.PriceRepository;
import kairos.pruebatecnica.domain.Price;
import kairos.pruebatecnica.domain.exception.PriceNotFoundException;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.*;


@AllArgsConstructor
public class PriceUseCase {

    private PriceRepository priceRepository;

    public Price getPricebyBrandProductAndDate(Long brandId, Long productId, LocalDateTime date){

        List<Price> prices = priceRepository.getPrices(brandId, productId);

        if(prices.isEmpty()){
            throw new PriceNotFoundException("Precio no encontrado", brandId, productId, date);
        }

        Optional<Price> priceOpt   = prices.stream()
                .filter(p -> (p.getStartDate().isBefore(date) && p.getEndDate().isAfter(date)))
                .max(Comparator.comparing(Price::getPriority));

        return priceOpt.orElseThrow(() -> new PriceNotFoundException("Precio no encontrado", brandId, productId, date));
    }
}


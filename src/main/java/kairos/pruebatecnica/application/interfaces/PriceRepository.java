package kairos.pruebatecnica.application.interfaces;

import kairos.pruebatecnica.domain.Price;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


public interface PriceRepository {

    List<Price> getPrices(Long brandId, Long productId);
}

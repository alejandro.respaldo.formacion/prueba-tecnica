package kairos.pruebatecnica.application.services;

import kairos.pruebatecnica.application.interfaces.PriceRepository;
import kairos.pruebatecnica.domain.Price;
import kairos.pruebatecnica.domain.exception.PriceNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;

@SpringBootTest
public class UseCaseTests {
    private Long brandId = 1L;
    private Long productId = 35455L;

    private List<Price> prices = Arrays.asList(
            new Price(LocalDateTime.of(2020,06,14,00,00,00),
                    LocalDateTime.of(2020,12,31,23,59,59),
                    35.50,"EUR",35455L,1L, 0),
            new Price(LocalDateTime.of(2020,06,14,15,00,00),
                    LocalDateTime.of(2020,06,14,18,30,00),
                    25.45,"EUR",35455L,1L, 1),
            new Price(LocalDateTime.of(2020,06,15,00,00,00),
                    LocalDateTime.of(2020,06,15,11,00,00),
                    30.50,"EUR",35455L,1L, 1),
            new Price(LocalDateTime.of(2020,06,15,16,00,00),
                    LocalDateTime.of(2020,12,31,23,59,59),
                    38.95,"EUR",35455L,1L, 1)
    );
    @Mock
    PriceRepository priceRepository;

    @InjectMocks
    PriceUseCase priceUseCase;

    @Test
    public void get_price_at_date_14_06_2020_T10_00(){
        LocalDateTime date = LocalDateTime.of(2020,6,14,10,00,00);

        when(priceRepository.getPrices(brandId,productId)).thenReturn(prices);

        Price price = priceUseCase.getPricebyBrandProductAndDate(brandId, productId, date);

        Assertions.assertEquals(35.50, price.getPrice());
    }
    @Test
    public void get_price_at_date_14_06_2020_T16_00(){
        LocalDateTime date = LocalDateTime.of(2020,6,14,16,00,00);
        when(priceRepository.getPrices(brandId,productId)).thenReturn(prices);

        Price price= priceUseCase.getPricebyBrandProductAndDate(brandId, productId, date);
        Assertions.assertEquals(25.45, price.getPrice());
    }

    @Test
    public void get_price_at_date_15_06_2020_T10_00(){
        LocalDateTime date = LocalDateTime.of(2020,6,15,10,00,00);
        when(priceRepository.getPrices(brandId,productId)).thenReturn(prices);

        Price price= priceUseCase.getPricebyBrandProductAndDate(brandId, productId, date);
        Assertions.assertEquals(30.50, price.getPrice());
    }
    @Test
    public void get_price_at_date_16_06_2020_T21_00(){
        LocalDateTime date = LocalDateTime.of(2020,6,16,21,00,00);

        when(priceRepository.getPrices(brandId,productId)).thenReturn(prices);

        Price price= priceUseCase.getPricebyBrandProductAndDate(brandId, productId, date);

        Assertions.assertEquals(38.95, price.getPrice());
    }

    @Test()
    public void get_price_at_date_not_exist(){
        LocalDateTime date = LocalDateTime.of(2020,5,14,21,00,00);

        when(priceRepository.getPrices(brandId,productId)).thenReturn(prices);

        Assertions.assertThrows(PriceNotFoundException.class, ()->priceUseCase.getPricebyBrandProductAndDate(brandId, productId, date));

    }
    @Test()
    public void get_price_at_product_not_exist(){
        LocalDateTime date = LocalDateTime.of(2020,5,14,21,00,00);

        when(priceRepository.getPrices(brandId,productId)).thenReturn(List.of());

        Assertions.assertThrows(PriceNotFoundException.class, ()->priceUseCase.getPricebyBrandProductAndDate(brandId, productId, date));

    }
}

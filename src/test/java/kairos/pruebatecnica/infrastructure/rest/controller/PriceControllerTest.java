package kairos.pruebatecnica.infrastructure.rest.controller;

import kairos.pruebatecnica.infrastructure.rest.dto.PriceDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
class PriceControllerTest {

    @Autowired
    WebApplicationContext webApplicationContext;
    private MockMvc mockMvc;

    @BeforeEach
    public void setup(){
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    void getPriceByBrandProductAndDate() throws Exception {

        DateTimeFormatter iso = DateTimeFormatter.ISO_LOCAL_DATE_TIME;

        PriceDto dto = new PriceDto(1L,
                LocalDateTime.of(2020,06,14,00,00,00),
                LocalDateTime.of(2020,12,31,23,59,59),
                35455L,35.5);


        mockMvc.perform(get("/prices/brands/{brandId}/products/{productId}",1,35455)
                .param("date","2020-06-14T10-00-00"))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.brandId").value(dto.getBrandId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.starDate").value(iso.format(dto.getStarDate())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.endDate").value(iso.format(dto.getEndDate())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.productId").value(dto.getProductId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.price").value(dto.getPrice()));
    }

    @Test
    void Not_found_Price() throws Exception {
        mockMvc.perform(get("/prices/brands/{brandId}/products/{productId}",2,35455)
                        .param("date","2020-06-14T10-00-00"))
                .andExpect(status().isNotFound())
                .andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Precio no encontrado"));

    }


}